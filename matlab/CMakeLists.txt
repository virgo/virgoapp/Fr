# -- Matlab library ---------

if (ENABLE_MATLAB)

    if (NOT DEFINED MATLAB_DATA_DIR)
        set(MATLAB_DATA_DIR ${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}/src/matlab)
    endif()

    set(FRAMEL_MATLAB_FILES
        frextract.c
        frextract.m
        frgetvect.c
        frgetvect.m
        frgetvectN.c
        frgetvectN.m
        mkframe.c
        mkframe.m
        mkframe_simevt.m
    )

    install(
        FILES ${FRAMEL_MATLAB_FILES}
        DESTINATION ${MATLAB_DATA_DIR}
    )

    if (NOT DEFINED ENABLE_MATLAB_MEX_COMPILE OR ENABLE_MATLAB_MEX_COMPILE)
        find_package(Matlab REQUIRED)

        include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../src)

        matlab_add_mex(
            NAME frextract
            SRC frextract.c
            LINK_TO framel
        )
        matlab_add_mex(
            NAME frgetvect
            SRC frgetvect.c
            LINK_TO framel
        )
        matlab_add_mex(
            NAME frgetvectN
            SRC frgetvectN.c
            LINK_TO framel
        )

        matlab_add_mex(
            NAME mkframe
            SRC mkframe.c
            LINK_TO framel
        )

        install(
            TARGETS frextract frgetvect frgetvectN mkframe
            LIBRARY DESTINATION ${MATLAB_DATA_DIR}
        )
    endif()
endif()
