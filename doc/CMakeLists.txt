# -- Documentation ----------

if (NOT DEFINED ENABLE_C OR ENABLE_C)
    file(GLOB LIBFRAME_DOCS *.pdf)
    install(
        FILES ${LIBFRAME_DOCS}
        DESTINATION ${CMAKE_INSTALL_DOCDIR}
    )
endif()
